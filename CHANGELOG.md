# Changelog


## (unreleased)

### New

#### Expand ignored files.


~ Jorge Araya Navarro

#### Mine porn directory 'theporndude' and format as /etc/hosts.


~ Jorge Araya Navarro

#### Add Colly for web scraping.


  http://go-colly.org/

~ Jorge Araya Navarro

#### Implement scraper that search for links in website.


  Divide the domain of concerns for the actual worker function that will process input and manage the
  output of the scraper function

  refs #2

~ Jorge Araya Navarro

#### Add a license and a read me file.


~ Jorge Araya Navarro

### Changes

#### Remove files.


~ Jorge Araya Navarro


