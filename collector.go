package main

import (
	"encoding/csv"
	"log"
	"os"
	"strings"

	"github.com/gocolly/colly"
)

// Blacklister defines an uniform type for all blacklist collectors
type Blacklister interface {
	Close()
	GetColly() *colly.Collector
}

type ETCHostsBlackLister struct {
	file      *os.File
	writer    *csv.Writer
	collector *colly.Collector
}

func NewETCHostsBlackLister(filename string) (ETCHostsBlackLister, error) {
	var blacklister ETCHostsBlackLister
	var err error
	blacklister.file, err = os.Create(filename)
	if err != nil {
		return ETCHostsBlackLister{}, err
	}
	blacklister.writer = csv.NewWriter(blacklister.file)
	blacklister.writer.Comma = '\t'
	// Insert the headers
	_ = blacklister.writer.Write([]string{"# Default IP", "Blocked site"})

	// Take care of blacklisting the hosts
	blacklister.collector = colly.NewCollector()
	// Write each visited link
	blacklister.collector.OnResponse(func(r *colly.Response) {
		// Register the base host name, i.e: somehost.com
		blacklister.writer.Write([]string{"127.0.0.1", r.Request.URL.Host})
		log.Printf("Blacklisted: %q", r.Request.URL.Host)
		if !strings.HasPrefix(r.Request.URL.Host, "www.") {
			// Register the base host name with www., i.e: www.somehost.com
			blacklister.writer.Write([]string{"127.0.0.1", "www." + r.Request.URL.Host})
			log.Printf("Blacklisted: %q", "www."+r.Request.URL.Host)
		}
		blacklister.writer.Flush()
		err := blacklister.writer.Error()
		if err != nil {
			log.Fatalf("Error occurred when writting to file %s: %s", filename, err)
		}
	})
	return blacklister, nil
}

func (etc ETCHostsBlackLister) GetColly() *colly.Collector {
	return etc.collector
}

func (etc ETCHostsBlackLister) Close() {
	etc.writer.Flush()
	etc.file.Close()
}
